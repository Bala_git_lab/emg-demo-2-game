﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WierdLightEffect : MonoBehaviour {

    private float direction;
    private float initial_z;

    private const float FRONT = 4f;
    private const float BACK = -4f;
    private const float SPEED = 0.5f;

    // Use this for initialization
    void Start()
    {
        direction = BACK;
        initial_z = gameObject.transform.position.z;
    }

    // Update is called once per frame
    void Update()
    {
        if ((gameObject.transform.position.z < -18.9 && gameObject.transform.position.z > -19) || (gameObject.transform.position.z > -15.1 && gameObject.transform.position.z < -15))
        {
            if (direction == FRONT)
                direction = BACK;
            else
                direction = FRONT;
            initial_z = gameObject.transform.position.z;
        }
        if(direction == BACK)
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, -19), Time.deltaTime * SPEED);
        else
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, -15), Time.deltaTime * SPEED);
    }
}
