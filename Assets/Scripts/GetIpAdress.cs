﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetIpAdress : MonoBehaviour {

    public InputField ip_input_field;

    public void setIpAdress()
    {
        Client.WebServiceURL = ip_input_field.text;
        Debug.Log(Client.WebServiceURL);
        Application.LoadLevel(1);
    }
}
