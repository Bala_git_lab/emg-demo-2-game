﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour {

    private float speed;
    private float rotation_speed;
    private int current_direction;       //can be '1': west, '2': north, '3': east, '4': south
    public static Vector3 moving_direction;
    private bool rotation_finished;
    private float rotation_update_time;
    private bool is_jumping;
    private bool is_falling;
    private float bonus_gravity;
    private float max_height;
    private bool gameOver = false;
    public GameObject bullet;
    public static int label;
    public static int previos_label;
    public Text highscore_text;
    private string highscore_key;
    public Text distance_text;
    private int distance;

    public static bool junction_collision_active = false;
    public static Vector3 new_player_position_in_junction;
    // Use this for initialization
    void Start () {

        current_direction = 2;
        moving_direction = new Vector3(0, 0, 1);
        rotation_speed = 20f;
        speed = 12f;
        rotation_update_time = 0.01f;
        bonus_gravity = 80f;
        rotation_finished = false;
        is_jumping = false;
        is_falling = false;
        max_height = 0;
        label = -1;
        previos_label = -1;
        distance = 0;
        rotation_finished = true;

        highscore_key = "highscore";
        highscore_text.text = "Highscore: " + PlayerPrefs.GetInt(highscore_key, 0);
    }
	
	// Update is called once per frame
	void Update () {

        distance++;
        distance_text.text = "Distance: " + distance.ToString() + " m";

        if(gameObject.transform.position.y - ProceduralWorldGenerating.groundArray[6].gameObject.transform.position.y < -2)
        {
            int current_highscore = PlayerPrefs.GetInt(highscore_key);
            if(distance > current_highscore)
            {
                PlayerPrefs.SetInt(highscore_key, distance);
                PlayerPrefs.Save();
            }
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }

        if (is_jumping)
        {
            Vector3 v = gameObject.GetComponent<Rigidbody>().velocity;
            v.y -= bonus_gravity * Time.deltaTime;
            switch (current_direction)
            {
                case 1:     //looking west
                    v.x -= 30 * Time.deltaTime;
                    break;
                case 2:     //looking north
                    v.z += 30 * Time.deltaTime;
                    break;
                case 3:     //looking east
                    v.x += 30 * Time.deltaTime;
                    break;
                case 4:     //looking south
                    v.z -= 30 * Time.deltaTime;
                    break;
                default:
                    break;
            }
            gameObject.GetComponent<Rigidbody>().velocity = v;
        }
        
       

        if (!is_jumping)
            gameObject.transform.Translate(moving_direction * speed * Time.deltaTime);



        if (label != previos_label && label == 2)       //going left
        {
            if (PlayerMovement.junction_collision_active == true)
            {
                gameObject.transform.position = PlayerMovement.new_player_position_in_junction;
            }
            previos_label = label;
            switch(current_direction)
            {
                case 1:     //I am moving west
                    moving_direction = new Vector3(0, 0, -1);
                    current_direction = 4;// I am now looking south
                    gameObject.transform.Find("Main Camera").gameObject.transform.position = new Vector3(gameObject.transform.Find("Main Camera").gameObject.transform.position.x - 10, gameObject.transform.Find("Main Camera").gameObject.transform.position.y, gameObject.transform.Find("Main Camera").gameObject.transform.position.z + 10);
                    rotation_finished = false;
                    StartCoroutine(rotate("left", "west"));
                    break;
                case 2:     //I am moving north
                    moving_direction = new Vector3(-1, 0, 0);
                    current_direction = 1;//I am now looking west
                    gameObject.transform.Find("Main Camera").gameObject.transform.position = new Vector3(gameObject.transform.Find("Main Camera").gameObject.transform.position.x + 10, gameObject.transform.Find("Main Camera").gameObject.transform.position.y, gameObject.transform.Find("Main Camera").gameObject.transform.position.z + 10);
                    rotation_finished = false;
                    StartCoroutine(rotate("left","north"));//gameObject.transform.Find("Main Camera").gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.Find("Main Camera").gameObject.transform.rotation, Quaternion.Euler(0, -90, 0), Time.deltaTime * rotation_speed);
                    break;
                case 3:     //I am moving east
                    moving_direction = new Vector3(0, 0, 1);
                    current_direction = 2;//I am now looking north
                    gameObject.transform.Find("Main Camera").gameObject.transform.position = new Vector3(gameObject.transform.Find("Main Camera").gameObject.transform.position.x + 10, gameObject.transform.Find("Main Camera").gameObject.transform.position.y, gameObject.transform.Find("Main Camera").gameObject.transform.position.z - 10);
                    rotation_finished = false;
                    StartCoroutine(rotate("left", "east"));
                    break;
                case 4:     //I am moving south
                    moving_direction = new Vector3(1, 0, 0);
                    current_direction = 3;//I am now looking east
                    gameObject.transform.Find("Main Camera").gameObject.transform.position = new Vector3(gameObject.transform.Find("Main Camera").gameObject.transform.position.x - 10, gameObject.transform.Find("Main Camera").gameObject.transform.position.y, gameObject.transform.Find("Main Camera").gameObject.transform.position.z - 10);
                    rotation_finished = false;
                    StartCoroutine(rotate("left", "south"));
                    break;
                default:
                    break;
            }
        }

        if (label != previos_label && label == 4 )//move right
        {
            if (PlayerMovement.junction_collision_active == true)
            {
                gameObject.transform.position = PlayerMovement.new_player_position_in_junction;
            }
            previos_label = label;
            switch (current_direction)
            {
                case 1:     //I am moving west
                    moving_direction = new Vector3(0, 0, 1);
                    current_direction = 2;// I am now looking north
                    gameObject.transform.Find("Main Camera").gameObject.transform.position = new Vector3(gameObject.transform.Find("Main Camera").gameObject.transform.position.x - 10, gameObject.transform.Find("Main Camera").gameObject.transform.position.y, gameObject.transform.Find("Main Camera").gameObject.transform.position.z - 10);
                    rotation_finished = false;
                    StartCoroutine(rotate("right", "west"));
                    break;
                case 2:     //I am moving north
                    moving_direction = new Vector3(1, 0, 0);
                    current_direction = 3;//I am now looking east
                    gameObject.transform.Find("Main Camera").gameObject.transform.position = new Vector3(gameObject.transform.Find("Main Camera").gameObject.transform.position.x - 10, gameObject.transform.Find("Main Camera").gameObject.transform.position.y, gameObject.transform.Find("Main Camera").gameObject.transform.position.z + 10);
                    rotation_finished = false;
                    StartCoroutine(rotate("right", "north"));
                    break;
                case 3:     //I am moving east
                    moving_direction = new Vector3(0, 0, -1);
                    current_direction = 4;//I am now looking south
                    gameObject.transform.Find("Main Camera").gameObject.transform.position = new Vector3(gameObject.transform.Find("Main Camera").gameObject.transform.position.x + 10, gameObject.transform.Find("Main Camera").gameObject.transform.position.y, gameObject.transform.Find("Main Camera").gameObject.transform.position.z + 10);
                    rotation_finished = false;
                    StartCoroutine(rotate("right", "east"));
                    break;
                case 4:     //I am moving south
                    moving_direction = new Vector3(-1, 0, 0);
                    current_direction = 1;//I am now looking west
                    gameObject.transform.Find("Main Camera").gameObject.transform.position = new Vector3(gameObject.transform.Find("Main Camera").gameObject.transform.position.x + 10, gameObject.transform.Find("Main Camera").gameObject.transform.position.y, gameObject.transform.Find("Main Camera").gameObject.transform.position.z - 10);
                    rotation_finished = false;
                    StartCoroutine(rotate("right", "south"));
                    break;
                default:
                    break;
            }
        }
        if (label != previos_label && label == 1 && !is_jumping)    //jump
        {
            previos_label = label;
            switch (current_direction)
            {
                case 1:     //I am moving west
                    gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, 40, 0), ForceMode.Impulse);
                    is_jumping = true;
                    break;
                case 2:     //I am moving north
                    gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, 40, 0), ForceMode.Impulse);
                    is_jumping = true;
                    break;
                case 3:     //I am moving east
                    gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, 40, 0), ForceMode.Impulse);
                    is_jumping = true;
                    break;
                case 4:     //I am moving south
                    gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, 40, 0), ForceMode.Impulse);
                    is_jumping = true;
                    break;
                default:
                    break;
            }
        }


        if (previos_label != label && label == 5)    //shoot bullet
        {
            previos_label = label;
            Instantiate(bullet, gameObject.transform.position + (moving_direction * 2), Quaternion.identity);
        }
        
        if(previos_label != label && label == 0)
        {
            previos_label = label;
        }
    }


    IEnumerator rotate(string direction_change, string looking_direction)
    {
        while (!rotation_finished)
        {
            switch (direction_change)
            {
                case "left":
                    switch (looking_direction)
                    {
                        case "west":
                            gameObject.transform.Find("Main Camera").gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.Find("Main Camera").gameObject.transform.rotation, Quaternion.Euler(0, -180, 0), Time.deltaTime * rotation_speed);
                            if (Quaternion.Angle(gameObject.transform.Find("Main Camera").gameObject.transform.rotation, Quaternion.Euler(0, -180, 0)) <= 1)
                            {
                                rotation_finished = true;
                                gameObject.transform.Find("Main Camera").gameObject.transform.rotation = Quaternion.Euler(0, -180, 0);
                               // Debug.Log("AM IESIT DIN ROTATIE");
                            }
                            break;
                        case "north":
                            gameObject.transform.Find("Main Camera").gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.Find("Main Camera").gameObject.transform.rotation, Quaternion.Euler(0, -90, 0), Time.deltaTime * rotation_speed);
                            if (Quaternion.Angle(gameObject.transform.Find("Main Camera").gameObject.transform.rotation, Quaternion.Euler(0, -90, 0)) <= 1)
                            {
                                rotation_finished = true;
                                gameObject.transform.Find("Main Camera").gameObject.transform.rotation = Quaternion.Euler(0, -90, 0);
                                //Debug.Log("AM IESIT DIN ROTATIE");
                            }
                            break;
                        case "east":
                            gameObject.transform.Find("Main Camera").gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.Find("Main Camera").gameObject.transform.rotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * rotation_speed);
                            if (Quaternion.Angle(gameObject.transform.Find("Main Camera").gameObject.transform.rotation, Quaternion.Euler(0, 0, 0)) <= 1)
                            {
                                rotation_finished = true;
                                gameObject.transform.Find("Main Camera").gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
                               // Debug.Log("AM IESIT DIN ROTATIE");
                            }
                            break;
                        case "south":
                            gameObject.transform.Find("Main Camera").gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.Find("Main Camera").gameObject.transform.rotation, Quaternion.Euler(0, 90, 0), Time.deltaTime * rotation_speed);
                            if (Quaternion.Angle(gameObject.transform.Find("Main Camera").gameObject.transform.rotation, Quaternion.Euler(0, 90, 0)) <= 1)
                            {
                                rotation_finished = true;
                                gameObject.transform.Find("Main Camera").gameObject.transform.rotation = Quaternion.Euler(0, 90, 0);
                                //Debug.Log("AM IESIT DIN ROTATIE");
                            }
                            break;
                        default:
                            break;
                    }
                    break;



                case "right":
                    switch (looking_direction)
                    {
                        case "west":
                            gameObject.transform.Find("Main Camera").gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.Find("Main Camera").gameObject.transform.rotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * rotation_speed);
                            if (Quaternion.Angle(gameObject.transform.Find("Main Camera").gameObject.transform.rotation, Quaternion.Euler(0, 0, 0)) <= 1)
                            {
                                rotation_finished = true;
                                gameObject.transform.Find("Main Camera").gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
                                //Debug.Log("AM IESIT DIN ROTATIE");
                            }
                            break;
                        case "north":
                            gameObject.transform.Find("Main Camera").gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.Find("Main Camera").gameObject.transform.rotation, Quaternion.Euler(0, 90, 0), Time.deltaTime * rotation_speed);
                            if (Quaternion.Angle(gameObject.transform.Find("Main Camera").gameObject.transform.rotation, Quaternion.Euler(0, 90, 0)) <= 1)
                            {
                                rotation_finished = true;
                                gameObject.transform.Find("Main Camera").gameObject.transform.rotation = Quaternion.Euler(0, 90, 0);
                                //Debug.Log("AM IESIT DIN ROTATIE");
                            }
                            break;
                        case "east":
                            gameObject.transform.Find("Main Camera").gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.Find("Main Camera").gameObject.transform.rotation, Quaternion.Euler(0, 180, 0), Time.deltaTime * rotation_speed);
                            if (Quaternion.Angle(gameObject.transform.Find("Main Camera").gameObject.transform.rotation, Quaternion.Euler(0, 180, 0)) <= 1)
                            {
                                rotation_finished = true;
                                gameObject.transform.Find("Main Camera").gameObject.transform.rotation = Quaternion.Euler(0, 180, 0);
                                //Debug.Log("AM IESIT DIN ROTATIE");
                            }      
                            break;
                        case "south":
                            gameObject.transform.Find("Main Camera").gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.Find("Main Camera").gameObject.transform.rotation, Quaternion.Euler(0, -90, 0), Time.deltaTime * rotation_speed);
                            if (Quaternion.Angle(gameObject.transform.Find("Main Camera").gameObject.transform.rotation, Quaternion.Euler(0, -90, 0)) <= 1)
                            {
                                rotation_finished = true;
                                gameObject.transform.Find("Main Camera").gameObject.transform.rotation = Quaternion.Euler(0, -90, 0);
                                //Debug.Log("AM IESIT DIN ROTATIE");
                            }
                            break;
                        default:
                            break;
                    }
                    break;



                default:
                   
                    break;
            }
                yield return new WaitForSeconds(rotation_update_time);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "ground" && is_jumping)
        {
            //Debug.Log("Touched the ground");
            is_jumping = false;
            is_falling = false;
            max_height = 0;
        }
        if(col.gameObject.tag == "obstacle")
        {
            //Debug.Log("Hit obstacle");
            int current_highscore = PlayerPrefs.GetInt(highscore_key);
            if (distance > current_highscore)
            {
                PlayerPrefs.SetInt(highscore_key, distance);
                PlayerPrefs.Save();
            }
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }
}
