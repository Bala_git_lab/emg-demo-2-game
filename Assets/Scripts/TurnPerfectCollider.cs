﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnPerfectCollider : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
    private void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "player")
        {
            PlayerMovement.junction_collision_active = true;
            PlayerMovement.new_player_position_in_junction = new Vector3(gameObject.transform.position.x, col.gameObject.transform.position.y, gameObject.transform.position.z);
        }
    }
    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "player")
        {
            PlayerMovement.junction_collision_active = false;
        }
    }
}
