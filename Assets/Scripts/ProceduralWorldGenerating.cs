﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralWorldGenerating : MonoBehaviour {

    public static GameObject[] groundArray;
    public GameObject ground;   //ground with obstacle
    public GameObject ground2;  //ground without obstacle
    public GameObject run_boost; //run boost

    public static bool run_boost_active;

    private const int OBSTACLE_PROBABILITY = 70;
    private const int RUN_BOOST_PROBABILITY = 40;
    private const float BOOST_OBJECT_ELEVATION = 3f;

    private int obstacle_probability;
    private int run_boost_probability;
    private int direction_change;       //can be '1': go left, '2': go forward, '3': go right
    private int curent_direction;       //can be '1': west, '2': north, '3': east, '4': south
    private int previos_direction_change;
    public GameObject player;
	// Use this for initialization
	void Start () {

        run_boost_active = false;
        groundArray = new GameObject[8];
        groundArray[0] = Instantiate(ground, new Vector3(0, 0, 20), Quaternion.identity);
        curent_direction = 2;
        previos_direction_change = -1;
        for (int i = 1; i < 8; i++)
        {
            run_boost_probability = Random.Range(1, 100);
            obstacle_probability = Random.Range(1, 100);

            //be sure that i don't go in circles
            direction_change = Random.Range(1,4);
            
            
            if (direction_change == 1 && direction_change == previos_direction_change)
            {
                direction_change = Random.Range(2, 4);
            }
            else
            {
                if(direction_change == 3 && direction_change == previos_direction_change)
                {
                    direction_change = Random.Range(1, 2);
                }
            }
            previos_direction_change = direction_change;
            


            //change direction and coordinates
            switch (direction_change)
            {
                case 1:     //go left
                    switch(curent_direction)
                    {
                        case 1:     //I am looking 
                            if(obstacle_probability > OBSTACLE_PROBABILITY)
                                groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i-1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2 + 2.5f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
                            else
                                groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2 + 2.5f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;

                            if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2 + 2.5f), Quaternion.Euler(new Vector3(0, 0, 0)));

                            curent_direction = 4;
                            break;
                        case 2:     //I am looking north
                            if(obstacle_probability > OBSTACLE_PROBABILITY)
                                groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2 + 2.5f, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;
                            else
                                groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2 + 2.5f, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;

                            if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2 + 2.5f, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0)));

                            curent_direction = 1;
                            break;
                        case 3:     //I am looking east
                            if (obstacle_probability > OBSTACLE_PROBABILITY)
                                groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2 - 2.5f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
                            else
                                groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2 - 2.5f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;

                            if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2 - 2.5f), Quaternion.Euler(new Vector3(0, 0, 0)));

                            curent_direction = 2;
                            break;
                        case 4:     //I am looking south
                            if (obstacle_probability > OBSTACLE_PROBABILITY)
                                groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2 - 2.5f, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;
                            else
                                groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2 - 2.5f, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;

                            if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2 - 2.5f, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0)));

                            curent_direction = 3;
                            break;
                        default:
                            break;
                    }
                    break;
                case 2:     //go forward
                    switch (curent_direction)
                    {
                        case 1:     //I am looking west
                            if (obstacle_probability > OBSTACLE_PROBABILITY)
                                groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;
                            else
                                groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;

                            if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z), Quaternion.Euler(new Vector3(0, 90, 0)));

                            curent_direction = 1;
                            break;
                        case 2:     //I am looking north
                            if (obstacle_probability > OBSTACLE_PROBABILITY)
                                groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
                            else
                                groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;

                            if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 0, 0)));

                            curent_direction = 2;
                            break;
                        case 3:     //I am looking east
                            if (obstacle_probability > OBSTACLE_PROBABILITY)
                                groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;
                            else
                                groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;

                            if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z), Quaternion.Euler(new Vector3(0, 90, 0)));


                            curent_direction = 3;
                            break;
                        case 4:     //I am looking south
                            if (obstacle_probability > OBSTACLE_PROBABILITY)
                                groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
                            else
                                groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;

                            if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 0, 0)));

                            curent_direction = 4;
                            break;
                        default:
                            break;
                    }
                    break;
                case 3:     //go right
                    switch (curent_direction)
                    {
                        case 1:     //I am looking west
                            if (obstacle_probability > OBSTACLE_PROBABILITY)
                                groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2 - 2.5f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
                            else
                                groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2 - 2.5f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;

                            if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2 - 2.5f), Quaternion.Euler(new Vector3(0, 0, 0)));

                            curent_direction = 2;
                            break;
                        case 2:     //I am looking north
                            if (obstacle_probability > OBSTACLE_PROBABILITY)
                                groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2 - 2.5f, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;
                            else
                                groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2 - 2.5f, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;

                            if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2 - 2.5f, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0)));


                            curent_direction = 3;
                            break;
                        case 3:     //I am looking east
                            if (obstacle_probability > OBSTACLE_PROBABILITY)
                                groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2 + 2.5f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
                            else
                                groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2 + 2.5f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;

                            if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2 + 2.5f), Quaternion.Euler(new Vector3(0, 0, 0)));


                            curent_direction = 4;
                            break;
                        case 4:     //I am looking south
                            if (obstacle_probability > OBSTACLE_PROBABILITY)
                                groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2 + 2.5f, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;
                            else
                                groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2 + 2.5f, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;

                            if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2 + 2.5f, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0)));

                            curent_direction = 1;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                        break;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
       
        if (Vector3.Distance(player.gameObject.transform.position, groundArray[3].gameObject.transform.position) <= 20f)
        {
            Destroy(groundArray[0]);
            for (int i = 0; i < 7; i++)
            {
                groundArray[i] = groundArray[i + 1];
            }



            //GENERATING NEW TILE/TILES

            for (int i = 7; i <= 7; i++)
            {
                run_boost_probability = Random.Range(1, 100);
                obstacle_probability = Random.Range(1, 100);
                //be sure that i don't go in circles
                direction_change = Random.Range(1, 4);


                if (direction_change == 1 && direction_change == previos_direction_change)
                {
                    direction_change = Random.Range(2, 4);
                }
                else
                {
                    if (direction_change == 3 && direction_change == previos_direction_change)
                    {
                        direction_change = Random.Range(1, 2);
                    }
                }
                previos_direction_change = direction_change;



                //change direction and coordinates
                switch (direction_change)
                {
                    case 1:     //go left
                        switch (curent_direction)
                        {
                            case 1:     //I am looking west
                                if (obstacle_probability > OBSTACLE_PROBABILITY)
                                    groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2 + 2.5f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
                                else
                                    groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2 + 2.5f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;

                                if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                    Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2 + 2.5f), Quaternion.Euler(new Vector3(0, 0, 0)));


                                curent_direction = 4;
                                break;
                            case 2:     //I am looking north
                                if (obstacle_probability > OBSTACLE_PROBABILITY)
                                    groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2 + 2.5f, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;
                                else
                                    groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2 + 2.5f, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;

                                if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                    Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2 + 2.5f, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0)));


                                curent_direction = 1;
                                break;
                            case 3:     //I am looking east
                                if (obstacle_probability > OBSTACLE_PROBABILITY)
                                    groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2 - 2.5f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
                                else
                                    groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2 - 2.5f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;

                                if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                    Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2 - 2.5f), Quaternion.Euler(new Vector3(0, 0, 0)));


                                curent_direction = 2;
                                break;
                            case 4:     //I am looking south
                                if (obstacle_probability > OBSTACLE_PROBABILITY)
                                    groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2 - 2.5f, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;
                                else
                                    groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2 - 2.5f, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;

                                if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                    Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2 - 2.5f, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0)));


                                curent_direction = 3;
                                break;
                            default:
                                break;
                        }
                        break;
                    case 2:     //go forward
                        switch (curent_direction)
                        {
                            case 1:     //I am looking west
                                if (obstacle_probability > OBSTACLE_PROBABILITY)
                                    groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;
                                else
                                    groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;

                                if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                    Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z), Quaternion.Euler(new Vector3(0, 90, 0)));


                                curent_direction = 1;
                                break;
                            case 2:     //I am looking north
                                if (obstacle_probability > OBSTACLE_PROBABILITY)
                                    groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
                                else
                                    groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;

                                if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                    Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 0, 0)));

                                curent_direction = 2;
                                break;
                            case 3:     //I am looking east
                                if (obstacle_probability > OBSTACLE_PROBABILITY)
                                    groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;
                                else
                                    groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;

                                if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                    Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z), Quaternion.Euler(new Vector3(0, 90, 0)));


                                curent_direction = 3;
                                break;
                            case 4:     //I am looking south
                                if (obstacle_probability > OBSTACLE_PROBABILITY)
                                    groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
                                else
                                    groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;

                                if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                    Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 0, 0)));


                                curent_direction = 4;
                                break;
                            default:
                                break;
                        }
                        break;
                    case 3:     //go right
                        switch (curent_direction)
                        {
                            case 1:     //I am looking west
                                if (obstacle_probability > OBSTACLE_PROBABILITY)
                                    groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2 - 2.5f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
                                else
                                    groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2 - 2.5f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;

                                if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                    Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2 - 2.5f), Quaternion.Euler(new Vector3(0, 0, 0)));

                                curent_direction = 2;
                                break;
                            case 2:     //I am looking north
                                if (obstacle_probability > OBSTACLE_PROBABILITY)
                                    groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2 - 2.5f, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;
                                else
                                    groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2 - 2.5f, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;

                                if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                    Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2 - 2.5f, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z + groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0)));


                                curent_direction = 3;
                                break;
                            case 3:     //I am looking east
                                if (obstacle_probability > OBSTACLE_PROBABILITY)
                                    groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2 + 2.5f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
                                else
                                    groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2 + 2.5f), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;

                                if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                    Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x + groundArray[i - 1].transform.localScale.z / 2, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2 + 2.5f), Quaternion.Euler(new Vector3(0, 0, 0)));


                                curent_direction = 4;
                                break;
                            case 4:     //I am looking south
                                if (obstacle_probability > OBSTACLE_PROBABILITY)
                                    groundArray[i] = Instantiate(ground, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2 + 2.5f, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;
                                else
                                    groundArray[i] = Instantiate(ground2, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2 + 2.5f, groundArray[i - 1].transform.position.y, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0))) as GameObject;

                                if (run_boost_probability < RUN_BOOST_PROBABILITY && obstacle_probability < OBSTACLE_PROBABILITY)
                                    Instantiate(run_boost, new Vector3(groundArray[i - 1].transform.position.x - groundArray[i - 1].transform.localScale.z / 2 + 2.5f, groundArray[i - 1].transform.position.y + BOOST_OBJECT_ELEVATION, groundArray[i - 1].transform.position.z - groundArray[i - 1].transform.localScale.z / 2), Quaternion.Euler(new Vector3(0, 90, 0)));


                                curent_direction = 1;
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
            
        }
        
	}
}
