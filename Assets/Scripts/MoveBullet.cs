﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBullet : MonoBehaviour {

    private float bullet_speed;
    private Vector3 bullet_moving_direction;
	// Use this for initialization
	void Start () {
        bullet_speed = 70f;
        bullet_moving_direction = PlayerMovement.moving_direction;
	}
	
	// Update is called once per frame
	void Update () {
        gameObject.transform.Translate(bullet_moving_direction * bullet_speed * Time.deltaTime);
	}
}
