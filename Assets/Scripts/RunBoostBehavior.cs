﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunBoostBehavior : MonoBehaviour
{
    private float direction;
    private float initial_y;

    private const float UP = 1.2f;
    private const float DOWN = -1.2f;
    private const float SPEED = 0.6f;

    // Use this for initialization
    void Start()
    {
        direction = UP;
        initial_y = gameObject.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        if(Mathf.Abs(gameObject.transform.position.y - (initial_y + direction) ) <= 0.03f)
        {
            if (direction == UP)
                direction = DOWN;
            else
                direction = UP;
            initial_y = gameObject.transform.position.y;
        }
        transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y + direction, transform.position.z), Time.deltaTime * SPEED);
    }
}
